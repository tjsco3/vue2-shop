# 禾木商城后台管理平台

此项目为电商后台管理人员使用，含商品，用户权限及订单管理等功能模块，实现商品销售相关信息展示，对管理用户账号，商品 分类，订单等数据进行展示及相关增删改查功能的实现；   

`技术栈：Vue2、vue-router、Axios、Echarts、Element-ui`

1. 使用vue-cli脚手架搭建项目框架，配置webpack打包工具及ElementUI等组件库； 
2. 使用vue-router进行页面路由配置，配置前置路由守卫进行页面鉴权，结合import实现组件懒加载; 
3. 配置基础url路径，超时等设置，封装axios请求完成前后端数据交互，渲染页面； 
4. 在请求拦截器中为请求头配置token进行身份验证，阻止用户非法登录； 
5. 通过Echarts的JS图表库配置，对后端数据进行数据可视化渲染；

## 登录页
![login](https://gitee.com/tjsco3/vue2-shop/raw/master/md-imgs/login.png)

## 首页
![home](https://gitee.com/tjsco3/vue2-shop/raw/master/md-imgs/home.png)

## 用户列表
![users](https://gitee.com/tjsco3/vue2-shop/raw/master/md-imgs/users.png)

## 权限管理
![rights](https://gitee.com/tjsco3/vue2-shop/raw/master/md-imgs/rights.png)

## 商品管理
![goods](https://gitee.com/tjsco3/vue2-shop/raw/master/md-imgs/goods.png)

## 添加商品
![goodsAdd](https://gitee.com/tjsco3/vue2-shop/raw/master/md-imgs/goodsAdd.png)

## 订单管理
![orders](https://gitee.com/tjsco3/vue2-shop/raw/master/md-imgs/orders.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
